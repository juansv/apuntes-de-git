## Índice

[Primera parte: Los conceptos](#primera-parte-los-conceptos)

> [Qué es git](#que-es-git)  
[Cómo se usa git](#como-se-usa-git)  
[Conceptos clave](#conceptos-clave)  
[La terminología de git](#los-conceptos-clave-con-la-terminologia-de-git)  
[Ejemplos gráficos](#ejemplos-que-ilustran-los-conceptos-claves)  
[Flujos de trabajo](#flujos-de-trabajo)  
[Fusión de ramas](#la-fusion-de-ramas)  
[Repositorios remotos](#los-repositorios-remotos)

[Segunda parte: El manejo](#segunda-parte-el-manejo)

> [Instalar y configurar git](#instalar-y-configurar-git)  
[Iniciar el repositorio local](#iniciar-el-repositorio-local)  
[Vincular con un repositorio remoto](#vincular-con-un-repositorio-remoto)  
[Hacer un commit](#hacer-un-commit)  
[Corregir un commit](#corregir-un-commit)  
[Crear ramas y moverse entre ramas y commits](#crear-ramas-y-moverse-entre-ramas-y-commits)  
[Obteniendo información](#obteniendo-informacion-el-log)  
[Subir y bajar del repositorio remoto](#subir-y-bajar-del-repositorio-remoto)  
[Fusionar ramas](#fusionar-ramas-hacer-merge)

# Primera parte: Los conceptos

## <a name="que-es-git"></a> Qué es git

Git es un sistema de gestión de versiones. Esto, dicho así, a mucha gente no le dirá nada. Lo mejor es usar un ejemplo: Supongamos que estamos escribiendo un cuento. En nuestra historia, en un momento dado, ocurre un hecho clave. Cuando terminamos escribir esa parte, la leemos y vemos que ha quedado muy bien, pero ocupa varias páginas. Para ser un cuento, nos ha quedado un poco largo y puede romper el ritmo de la narración. Pero tampoco sabemos si redactarlo de una forma más resumida tendrá la misma fuerza. Decidimos reescribir esta parte, pero sin perder lo que ya hemos escrito, por si acaso no nos convence el cambio. Acabamos de crear una versión de nuestro cuento.

Git no nos va a ayudar a escribir, ni nos dará ideas para la narración, ni nos resuelve dudas ortográficas... pero es capaz de recordar nuestra historia justo en el punto en que queremos valorar una alternativa. Y es capaz de devolver nuestra historia a ese punto inicial, cuando no habíamos cambiado una coma. No sólo eso, también es capaz de localizar las diferencias entre las dos ramas de la historia que creamos; y nos señala estas diferencias, lo que nos permite fusionar ambas o determinar con cuál de ellas nos queremos quedar. Y no sólo con dos ramas, git gestiona múltiples versiones. Incluso podríamos pedirle a un amigo que nos proponga una alternativa y luego valorar si la integramos y cómo.

Es decir, git es útil cuando hacemos algo que, como escribir o programar, evoluciona conforme lo desarrollamos. Una herramienta como git no tiene sentido para tareas con un desarrollo más simple, como montar un mueble o seguir una receta (si se pudiese usar git en estos casos). Como cualquier cosa en el mundo de la tecnología, git te puede ser de utilidad o no. Pero si te encaja, realmente te puede ayudar. Y ojo, el ejemplo del cuento no es ninguna tontería. Si usamos el ordenador para escribir git podría ayudarnos en el proceso creativo de una novela, por qué no.

## <a name="como-se-usa-git"></a> Cómo se usa git

Una vez que hemos visto para qué nos puede servir git, queda resolver cómo lo podemos usar. Por raro que parezca, no hay una forma concreta de usar git. Por supuesto, tiene unas instrucciones de manejo, como un coche. Pero, como ocurre con el coche, la manera en que hay que manejarlo no me obliga a ir a un sitio u otro; eso lo decido yo. No obstante, git no ofrece tanta flexibilidad y hay un puñado de formas típicas de uso. Es lo que se llaman los "flujos de trabajo" de git.

Por lo tanto, para entender bien el cómo, vamos a explicarlo de lo general a lo concreto. Vamos a ver en qué se basa git, como hace su trabajo y de ahí pasaremos a los flujos de trabajo más habituales y al manejo concreto de la herramienta git.

## Conceptos clave

- Git no hace "copias de seguridad" ni nada por el estilo. Git sólo recuerda cómo era tu trabajo en un momento dado. Es como si git sacase una "fotografía" del estado de tu trabajo en un momento dado.
- Yo, como usuario, decido cuándo se sacan esas "fotografías" del estado de mi trabajo. No lo hace git, lo hago yo. Por lo tanto, no puedo pedirle a git que recupere el estado de mi trabajo en un punto del que no haya sacado su correspondiente "foto".
- Con git me puedo mover entre las "fotografías" de mi trabajo. Puedo saltar de una a otra, yendo adelante o atrás en el *tiempo* (en el momento del desarrollo de mi trabajo). Puedo saltar de una en una, o de dos en dos. Puedo retroceder a la primera, o volver a la última.
- <a name="SHA"></a>Para saltar entre "fotografías", git simplemente les pone una etiqueta. Y yo le digo a git que me lleve al estado de mi trabajo que tiene la etiqueta "X" o la etiqueta "Y".
- <a name="ramas"></a>Las "fotografías" de la evolución de mi trabajo tienen una secuencia lógica: una se sacó antes y otra después. Por lo tanto, las distintas "fotografías" de mi trabajo están enlazadas, formando una cadena. Sin embargo, en un momento dado puedo hacer una variante de mi trabajo, como en el ejemplo del [cuento](#que-es-git). En ese instante, la secuencia de "fotos" se divide en dos. Y puede dividirse tantas veces como sea; como un árbol que se va ramificando.
- Git me permite saltar de "fotografía en fotografía", aunque estén en distintas ramas. La única salvedad es que también hay que ponerle una etiqueta a la rama, para indicarle a git con precisión a qué "foto" quiero ir.
- <a name="local"></a>Por último, y quizás es el concepto más importante, git sólo funciona en tu ordenador. Cuando vemos páginas web con proyectos git, estamos viendo el reflejo de un proyecto que usa git, pero que está en el ordenador de alguien. Git no es un servicio de internet, como el correo electrónico. Necesitas tener instalado git en tu ordenador para trabajar con git. Las páginas web como [gitlab](https://gitlab.com) o [github](https://github.com) permiten compartir tu trabajo y un resumen de las "fotografías" que has ido tomando, de tal forma que otras personas pueden ver cómo evoluciona tu trabajo. De esta forma varias personas pueden trabajar en un mismo proyecto. Pero puedes usar git y no tener cuenta en ninguna página web. En otras palabras: los servicios git por internet multiplican el potencial de git, pero si no tienes git en tu ordenador no puedes hacer nada.

## <a name="los-conceptos-clave-con-la-terminologia-de-git"></a> Los conceptos clave con la terminología de git

En el [capítulo anterior](#conceptos-clave) vimos como en git todo pivota alrededor de las "fotografías" del estado de nuestro trabajo en un momento dado. En la jerga de git, esas "fotografías" se llaman **commits**.

Commit es un término polisémico. Entre sus acepciones están *memorizar* y *anotar*. Estas dos son las que más aplicarían a su uso en git. Pero en algunos manuales y webs se usa "confirmación" como traducción de commit. En todo caso, lo más habitual, con mucha diferencia, es usar directamente el término *commit*. A partir de ahora, en este manual, y siguiendo la costumbre más habitual, dejaremos de usar la metáfora de las fotografías para usar el término **commit**.

También hemos visto que todos los commits tienen una [etiqueta](#SHA) que los identifica. Cuando hacemos un commit con git, automáticamente se le asigna esta etiqueta, que recibe el nombre de **SHA**. Existen comandos de git tanto para hacer un commit como para ver su SHA, pero eso lo veremos más adelante. Lo importante es que no nos tenemos que preocupar por etiquetar un commit, ni tampoco tenemos que anotar estas etiquetas en ningún lado, git las recuerda por nosotros.

También hemos visto que los commit están encadenados, unidos en una secuencia lógica. Pero esta cadena puede bifurcarse, formándose [**ramas**](#ramas). En la jerga de git, a las ramas se las llama así, **ramas**. El término en inglés para rama es **branch**.

Las ramas también están etiquetadas, como los commits. Las etiquetas que identifican una rama reciben el nombre técnico de *punteros*. Un puntero, como su nombre indica, apunta a un elemento concreto. Como quiera que en git los elementos centrales son los commits, los punteros de rama señalan a un commit en particular: al último commit de la cadena.

A diferencia de los commits, a las ramas hay que darles un nombre que las identifiquen, git no las nombra automáticamente. Se puede poner cualquier nombre a un puntero de rama, pero existe una convención para nombrar algunas ramas clave.

Para terminar, existe un puntero especial que no indica ninguna rama. Ese puntero tiene un nombre propio, **HEAD**. Este puntero señala al commit a partir del cual estamos trabajando. Y podemos moverlo al commit que queramos. Pero lo habitual es que *HEAD* esté en el último commit de una cadena. Es lógico que sea así, porque lo normal es que trabajemos haciendo un commit tras otro y, por lo tanto, siempre estamos trabajando sobre el último que hemos creado. De hecho, cuando hacemos un commit, el puntero *HEAD* se mueve automáticamente a este último commit.

> Nota: Cuando se invoca al puntero *HEAD* con un comando de git, se escribe así, con letras mayúsculas.

Tiene sentido saltar a un commit anterior si queremos crear una rama nueva en nuestro trabajo a partir de ese commit. Pero lo normal es que *HEAD* esté siempre al final de una rama. Por lo tanto, normalmente habrá una rama cuyo último commit tendrá dos punteros: el que da nombre a la rama y el puntero de trabajo *HEAD*.

## Ejemplos que ilustran los conceptos claves

Visualizar todo lo que hemos explicado hasta ahora sólo con palabras es difícil. Pero ilustrar estos conceptos gráficamente facilita mucho la comprensión. De hecho, es muy común que los manuales disponibles sobre git recurran a dibujos para explicar el proceso. Vamos a intentarlo.

### Qué es un commit, qué hace, cómo funciona

El siguiente gráfico ilustra un documento de texto al que se añaden y quitan palabras. En cada cambio, se hace un commit, para registrar esa modificación:

![](./esquemas/01-secuencia-commits.jpg "Secuencia de commits")

Si nos movemos de un commit a otro, nuestro documento cambiará y adoptará el texto que tenía **cuando se registró el commit**. Y eso es lo que veremos al abrir el archivo con nuestro editor de texto. Esta es la magia de git, el mismo documento tendrá un contenido u otro según el commit en el que estemos. El contenido cambia automáticamente. Por ejemplo, si nos movemos al commit 2 y abrimos el archivo, veremos esto:

![](./esquemas/02-archivo-commit-2.jpg "Archivo guardado con el commit 2")

Es decir, vemos lo que había **en el momento** de volcar el archivo al commit 2.

Ahora imaginemos que todavía no hemos hecho el último commit, el commit 3. Obviamente, estaremos en el commit 2; pero ya hemos modificado nuestro archivo, que tendrá el siguiente contenido:

![](./esquemas/03-archivo-commit-3.jpg "Archivo guardado con el commit 3")

En este punto, si intentamos saltar a otro commit (en el ejemplo, al commit 1) git nos devolverá un mensaje de error: No nos dejará movernos de commit mientras no confirmemos los cambios que hemos hecho mediante un commit nuevo, el que será el commit 3. Sin embargo, y esto es importante, los cambios quedarán guardados en nuestro directorio, en nuestro ordenador. Es decir, no es necesario hacer commit cada vez que apagamos nuestro ordenador. Los cambios no se pierden, la siguiente vez que encendamos el ordenador el archivo tendrá el mismo contenido que tenía cuando apagamos. Simplemente no podemos movernos a otro commit.

Cada vez que hacemos un nuevo commit, git se posiciona sobre él. En el ejemplo, en cuando hagamos el commit 3, git se moverá automáticamente del commit 2 al 3. Eso es transparente para nosotros, pero debemos tenerlo en cuenta.

### <a name="que-son-las-ramas-como-funcionan"></a> Qué son las ramas, cómo funcionan

Cuando trabajamos con git siempre estamos trabajando sobre un commit. Pero también siempre trabajamos en una rama. Si no le decimos sobre qué rama estamos trabajando, git creará una etiqueta de rama por defecto, que se llama rama *main*. Por lo tanto, nuestro dibujo anterior sería más correcto si tuviese este aspecto:

![](./esquemas/04-rama-main.jpg "Todo está en una rama")

Aparece un nuevo elemento: un puntero que señala al último commit. Ese puntero es el que da nombre a esa cadena de commits o **rama** en terminología de git. Como nuestro ejemplo es muy sencillo, sólo tenemos una rama, que por defecto se llama **main**, pero que podríamos haberle dado cualquier otro nombre.

> Nota: antiguamente se reservaba la palabra **master** en lugar de **main** para la rama principal. El uso de *master* está en desuso, pero todavía puede verse en muchos repositorios y manuales.

Si queremos hacer otra rama, deberemos seguir los siguientes pasos:

1. Primero, situarnos en el commit desde el que saldrá la nueva rama. En nuestro ejemplo, vamos a crear una nueva rama a partir del commit 2.
2. Luego, crearíamos el puntero de la nueva rama. En nuestro ejemplo, vamos a llamar a esta nueva rama *versión*
3. En ese momento, tendríamos dos punteros, ambos apuntando al último commit de su rama respectiva: El puntero *main* apuntando al commit 3 y el puntero *versión* apuntando al commit 2. El commit 2 es el primero y el último de la nueva cadena:
![](./esquemas/05-dos-punteros.jpg "Creamos un nuevo puntero de rama")
4. Ahora le decimos a git que queremos trabajar sobre la nueva rama que acabamos de crear.
5. A partir de este momento, los cambios que hagamos en nuestro archivo tomarán una nueva dirección, es decir, la nueva rama adquiere contenido propio:
![](./esquemas/06-dos-ramas.jpg "La nueva rama adquiere contenido")

Veamos con detalle qué ocurre con la nueva rama:

- En primer lugar, una vez que le decimos a git que queremos trabajar sobre la nueva rama, los cambios, y los commits que registran estos cambios, transcurren por *versión* y abandonan el camino abierto por la rama *main*.
- Cada vez que hacemos un commit sobre la nueva rama, el puntero de rama pasa automáticamente al último commit de la misma. En el ejemplo, se ilustra como se ha hecho el *commit 4* y el puntero *versión* pasó a señalar este commit. Esto es algo transparente para nosotros, lo hace git automáticamente.
- Los otros commits y ramas no desaparecen ni sufren ningún cambio. Simplemente no estamos trabajando sobre ellos. Podríamos volver a usarlos sin más que decirle a git que salte a la rama *main*.
- El commit 2 pertenece tanto a la rama *main* como a la rama *versión*. Es decir, el commit 2 es un punto común del que nacen ambas ramas. Parece una obviedad, pero es importante: Desde el momento que tenemos más de una rama no basta sólo con saber sobre qué commit estamos, también hay que saber sobre qué rama estamos. Git tiene herramientas para no perdernos, con un simple comando podemos saber nuestra rama y nuestro commit actual:
![](./esquemas/07-dos-ramas-commits.jpg "Los commits se ramifican")
- Nuestro documento ha adquirido dos versiones diferentes:
![](./esquemas/08-dos-ramas-archivos.jpg "El archivo tiene dos versiones")

### El puntero HEAD

Todo lo que hemos visto en el [capítulo anterior](#que-son-las-ramas-como-funcionan) habla de los punteros de rama, pero omite el puntero *HEAD*. Está hecho así para ver los conceptos poco a poco. Pero en git, siempre existen dos punteros como mínimo: El puntero de rama y el puntero *HEAD*. Obviamente, conforme creamos ramas se añade un puntero más por cada nueva rama.

El puntero *HEAD* soluciona saber sobre qué commit y qué rama estamos trabajando. Git tiene un comando para mover el puntero *HEAD* al commit o a la rama que queramos. Sin embargo, hay que tener en cuenta un detalle:

- Si le decimos a git que mueva el puntero *HEAD* **a una rama**, *HEAD* apuntará al último commit de esa rama.
- Si le decimos al git que mueva el puntero **a un commit**, *HEAD* apunta directamente a ese commit, **pero se desvincula de la rama**.

Es decir, mover el puntero *HEAD* a la rama *main* implica moverlo al commit 3:

![](./esquemas/09-HEAD-commit-3.jpg "HEAD en la rama main")

Y mover el puntero *HEAD* a la rama *versión* lo mueve al commit 4:

![](./esquemas/10-HEAD-commit-4.jpg "HEAD en la rama versión")

Pero si movemos el puntero *HEAD* directamente a un commit, aunque sea a los commits finales de rama, no cambiamos de rama. Por ejemplo, si estamos en la rama *main* y cambiamos al commit 4, no cambiamos a la rama *versión*. El puntero *HEAD* señalará al commit 4 pero los cambios que hagamos no se relacionarán con ninguna rama. Moverse a un commit determinado sirve para ver el estado de nuestro proyecto en ese momento o iniciar una nueva rama. Pero **para trabajar en una rama debemos mover *HEAD* a la rama**.

## Flujos de trabajo

Una vez que hemos comprendido cómo funciona git y tenemos claros los conceptos básicos, pasamos a ver algunos flujos de trabajo típicos. Un flujo de trabajo es como un guión que tenemos que seguir. No es obligatorio desde el punto de vista de git, es recomendable desde el punto de vista de la persona que utiliza git. Es decir, git es muy flexible: podemos crear un montón de versiones de nuestro trabajo, podemos hacer versiones de una versión, podemos trabajar sobre la rama *main* y sobre las distintas versiones saltando de una a otra, etc. Pero si actuamos sin un guión previo, sin una estrategia, acabaremos teniendo una complicada madeja de ramas y no sabremos muy bien sobre cuál de ellas avanzar. Estaremos perdidos y lo sufrirá nuestro proyecto. Y, desde luego, en ese caso git no nos habrá ayudado en absoluto.

### Flujo básico

Se trata de usar git de manera totalmente lineal, sin ramificaciones. Vamos haciendo cambios en nuestro trabajo y, cuando proceda, los confirmamos con un commit. Trabajamos todo el tiempo sobre la rama *main*.

Este flujo parece muy tonto, pero es muy adecuado cuando el trabajo lo hacemos nosotros solamente, no hay nadie más que participe. Al principio del proyecto definimos claramente lo que queremos hacer. Dividimos el proyecto en tareas simples y hacemos un commit cada vez que acabamos una de estas tareas. No nos planteamos alternativas ni variantes, nos atenemos a la idea inicial hasta terminar.

### Flujo de rama de funcionalidad

Cuando hablamos de funcionalidad, empezamos a hablar de un desarrollo de software. Pero vamos a poner un ejemplo más amistoso: supongamos que nuestro trabajo es escribir una novela. Tenemos una idea general de la historia que queremos contar, de los personajes, de alguna situación que queremos narrar, etc. Con esta idea general, estructuramos nuestra novela en capítulos, por ejemplo: El primer capítulo lo usaremos para introducir los personajes A y B, el segundo capítulo para contar el conflicto entre el personaje A y un tercer personaje C, y así sucesivamente. Una vez definidos los capítulos, creamos una rama para cada capítulo y nos ponemos a escribir. Si un día nos apetece más trabajar sobre el capítulo 1, lo hacemos. Si nos apetece más desarrollar el capítulo 5, lo hacemos. No hay problema. Cuando acabemos un capítulo, lo integramos en la rama *main*. Y así, capítulo a capítulo. Cuando terminemos, en *main* estará toda nuestra novela.

Si hablamos de desarrollar un software, se crean ramas específicas para cada funcionalidad de nuestro software. No trabajamos en la rama *main*. Trabajamos cada funcionalidad en su rama. Cuando la funcionalidad está terminada, la integramos en *main*.

Esta forma de trabajar es sencilla y permite que trabajen varias personas en el mismo proyecto, cada uno en una rama o funcionalidad. En ese caso, es interesante que alguno de los participantes asuma el rol de administrador. Es decir, que alguien asuma la responsabilidad de verificar que se han hecho las comprobaciones necesarias para que todas las ramas se integren sin problemas. Sin embargo, para este flujo no se considera obligatoria la figura del administrador.

### Flujo github

Es igual que el [flujo de trabajo de rama de funcionalidad](#flujo-de-rama-de-funcionalidad) pero en este caso la figura del Administrador de Integración es obligatorio. El Administrador no solo se encarga de asegurar que está todo probado, es también el que realiza la integración de todas las ramas en *main*. La rama *main* debe estar protegida y sólo puede trabajar sobre ella el Administrador.

### Flujo gitflow

Es un flujo más estricto que el [flujo github](#flujo-github). En el flujo gitflow, además de haber un Administrador de Integración, existe una **estructura de ramas predeterminada y cerrada**. Es decir, no podemos crear ramas a nuestro antojo. Las integraciones no se realizan directamente sobre la rama *main*, antes de pasarlas a *main* tuvieron que integrarse en otra rama llamada *develop* ("desarrollo"). De esta forma, en *main* siempre está una versión estable, productiva y protegida de nuestro software. Las funcionalidades sobre las que está desarrollando cuelgan de la rama *develop*.

Como se puede intuir, este es un flujo de trabajo profesional, propio de grupos de desarrollo de software. No vamos a entrar en más detalle. Existe abundante documentación en internet sobre este flujo de trabajo, que es fácil de entender una vez que se comprenden los principios básicos de git que se cuentan en el presente manual.

### Conclusión

Los flujos de trabajo que acabamos de ver son maneras habituales y más o menos estandarizadas de trabajar con git. Pero git es flexible y podemos diseñar nosotros nuestro propio flujo, uno que se adapte a nuestro proyecto y al equipo de personas que trabajará en él.

Pero la conclusión, lo realmente importante de todo lo que hemos visto, es que si nos ponemos a usar git sin una idea previa de nuestro flujo de trabajo, podemos meternos en un callejón sin salida. Es una muy buena práctica tener un flujo de trabajo en la mente antes de usar git.

En general, un flujo de trabajo debe respetar la rama *main* como la rama donde estará la versión final de nuestro trabajo; y todas las alternativas que queramos explorar, lo haremos en ramas diferentes de la *main*, hasta que estén lo bastante maduras para pasar a formar parte de la versión final.

## <a name="la-fusion-de-ramas"></a> La fusión de ramas

Hemos visto en los [flujos de trabajo](#flujos-de-trabajo) que la rama *main* se reserva para lo que es realmente nuestro proyecto, para la versión definitiva. También hemos hablado de la creación de ramas para el desarrollo de versiones y funcionalidades alternativas. Pero en algún momento deberemos tomar alguna de estas ramas y mezclarla con la rama *main*. Por ejemplo, si estamos escribiendo un cuento y hemos creado dos posibles finales, en algún momento deberemos elegir uno de los finales y terminar el cuento. O si estamos haciendo un software y tenemos una rama en la que hemos ido desarrollando una funcionalidad, en algún momento esta funcionalidad estará madura y podremos integrarla con el resto del programa. Es decir, en algún momento la rama con la versión o con la funcionalidad muere y pasa a formar parte de la rama *main*. En git, el ejercicio de fusionar unas ramas en otras recibe el nombre de **merge**.

Un **merge** se realiza desde una rama llamando al contenido de otra. Es decir, <a name="pasos-merge"></a> para hacer un merge tenemos que dar dos pasos:

1. Situarte en la rama que recibirá el contenido de la otra rama.
2. Traerse el contenido de la otra rama.

Por ejemplo, si estamos desarrollando una funcionalidad en la rama *nueva_función*, pero ya está terminada y queremos integrarla en *main*: Primero nos situamos en *main* y, desde ahí, hacemos **merge** con *nueva_función*.

El proceso de fusión o merge no es automático. Salvo que la fusión sea muy obvia, git solo nos ayuda a fusionar las ramas, pero el trabajo lo tenemos que rematar nosotros. Es decir, git detecta los cambios y mezcla el contenido de una rama en la otra, pero git no sabe si tiene que borrar o añadir algo más. En general, se pueden dar dos casos, que vemos a continuación. Identificamos estos casos con el nombre técnico que se utiliza en la jerga de git.

### Fast forward merge

En este caso una de las ramas, habitualmente la rama *main*, está parada, es decir, no tiene ningún commit desde que se creó la otra rama. Por lo tanto, el merge consiste en añadir las novedades que trae la nueva rama a la rama *main*. Pero, como *main* no tiene ningún commit propio, lo que hace git es mover el puntero *main* al último commit de la otra rama. Veamos un ejemplo:

![](./esquemas/11-fast-forward-merge-1.jpg "Nos salimos de main por la rama versión")

En el esquema anterior se ilustra cómo nos salimos de la rama *main* y derivamos nuestro proyecto por la rama *versión*. La rama *main* queda parada en el commit 2, no continúa su desarrollo. Sin embargo, la rama *versión* sí que progresa. Cuando termina el desarrollo de la rama *versión* (commit 4) hacemos la fusión con *main*. El merge debe llevar el último commit de *versión* a donde está el puntero de la rama *main*. Pero, si nos fijamos bien, el esquema de arriba puede dibujarse de otra forma:

![](./esquemas/12-fast-forward-merge-2.jpg "En realidad todos los commits siguen una misma línea")

Es decir, como *main* no continuó su desarrollo, en realidad todos los commits de *versión* están alineados con *main*. Por lo tanto, cuando hacemos **merge**, git sólo mueve el puntero *main* al final de la rama.

![](./esquemas/13-fast-forward-merge-3.jpg "Un Fast Forward merge es como mover el puntero main al final")

*Main* pasó adelante, que es el significado de fast forward. Sin embargo, al hacer un fast forward merge no se crea ningún commit nuevo, simplemente se desplaza el puntero. Esto tiene mucha importancia: al no haber ningún commit vinculado al merge, tenemos dos problemas:

1. No tenemos manera de documentar el merge. De hecho, la falta de un commit también hace más difícil determinar dónde empieza y termina la rama alternativa, en nuestro ejemplo la rama *versión*.
2. Tampoco tenemos manera de sincronizar el merge con un repositorio remoto.

Pero, aunque estemos en un escenario propio para el fast forward merge, le podemos indicar a git que no use la técnica fast forward. En este caso, git realiza el merge con un commit nuevo.

![](./esquemas/13.1-fast-forward-merge-4.jpg "Se puede omitir la técnica fast forward al hacer merge")

Este nuevo commit se crea automáticamente al hacer el merge con la opción de *omitir fast forward*. De esta forma la rama *versión* tiene claramente definido su principio y su final, disponemos de un commit nuevo en la rama *main* donde registrar los cambios que implica el merge y este commit nos permitirá subir los cambios a un repositorio remoto.

### Three way merge

En este caso, a diferencia del anterior, la rama *main* sí que tuvo cambios desde que nació la otra rama. Aquí la cosa se complica:

![](./esquemas/14-three-way-merge-antes.jpg "Las dos ramas han tenido desarrollo")

Cuando git tiene que fusionar dos ramas que han tenido su propio desarrollo independiente, utiliza un algoritmo llamado **three way merge**. Mediante este algoritmo, git modifica nuestros archivos, los del proyecto, añadiendo unas anotaciones que indican los cambios que ha habido y de qué rama proceden dichos cambios. Y registra estas anotaciones haciendo un commit. Este commit se crea automáticamente, sin que nosotros lo pidamos, al hacer el **merge**:

![](./esquemas/15-three-way-merge-despues.jpg "Se crea un nuevo commit tras el merge")

Somos nosotros los que, con ayuda de las anotaciones que hizo git, tenemos que decidir que cambios se quedan y que cambios no. Tenemos que editar nuestros archivos del proyecto y quitar y poner lo que consideremos oportuno. El algoritmo **three way merge** sólo hace de "secretario de actas", tomando nota ordenada de todos los cambios que trae cada rama, pero no toma decisiones por nosotros.

## Los repositorios remotos

Todo lo que hemos visto hasta ahora, tal y como se explicó en los [conceptos clave](#local), tiene sentido en nuestro ordenador. Ahora vamos a ver cómo podemos compartir este trabajo con otras personas usando un repositorio git remoto.

> Nota: El directorio del disco duro de mi ordenador donde estoy guardando mi proyecto se llama *repositorio local*.

Los repositorios remotos son espacios de almacenamiento que están disponibles en la nube y a los que accedemos usando comandos de git o con un navegador web. Vamos a entender muy bien qué son los repositorios remotos enunciando una serie de ideas clave:

- Al repositorio remoto no se suben archivos por copiar-pegar, como si fuese una carpeta de nuestro disco duro. Al repositorio remoto sólo puedo subir commits. Es decir, si hago un cambio en mi repositorio local, primero tengo que registrar los cambios en un commit antes de poder subir nada al repositorio remoto. Eso incluye la primera vez, cuando se crean los archivos.
- Podemos compartir nuestro proyecto en uno o en varios repositorios remotos a la vez. Del mismo modo, podemos descargarnos un proyecto que esté en un repositorio remoto a nuestro ordenador.
- El programa git **que tengo instalado en mi ordenador** identifica al repositorio remoto con un "alias". Por lo tanto, el primer paso para compartir un proyecto en un repositorio remoto es vincular el repositorio remoto con mi repositorio local e indicar el alias.
- Si mi proyecto está en varios repositorios remotos a la vez, tengo que asignarle un alias **diferente** a cada uno de ellos.
- Si mi proyecto está en varios repositorios remotos a la vez, cualquier cosa que quiera subir deberé hacerla **repositorio a repositorio**.
- Si bien puedo usar cualquier alias, existe una convención para nombrar los repositorios remotos: el repositorio remoto principal se suele llamar **origin**. Es una palabra clave reservada para este repositorio, de tal forma que, por omisión, se supone que subimos o nos descargamos proyectos desde **origin**.
- Para que varias personas trabajen a la vez en un mismo proyecto, por ejemplo, desarrollando cada una en una rama distinta, una de ellas deberá iniciar el proyecto en el repositorio remoto y las otras deberán descargarse el proyecto recién iniciado desde el repositorio remoto a su ordenador. Al acto de bajarse un proyecto que está en un repositorio remoto se le denomina **clonar**.
- Cuando varias personas trabajan en un mismo repositorio remoto, no es necesario que utilicen el mismo *alias*; pueden usar cada una un nombre diferente con el que referirse al repositorio remoto. El *alias* es una configuración particular de cada usuario en su equipo, no es una característica del repositorio remoto.

# Segunda parte: El manejo

Una vez vistos y entendidos los conceptos, pasamos a ver el manejo concreto de git. Lo haremos desde la consola de comandos.

## Instalar y configurar git

Según todo lo que se ha contado hasta ahora, git debe estar instalado en nuestro ordenador. Esto es condición *sine qua non*. La instalación de git dependerá tu sistema y en eso no nos vamos a meter, porque normalmente es muy sencillo.

Una vez instalado git, debemos configurarlo indicando nuestro nombre y nuestro correo electrónico. Para ello ejecutamos los comandos:

`git config --global user.name "nombre"`  
`git config --global user.email "nombre@correo.es"`

El nombre y correo se indican así, entre comillas. Esta configuración sólo hay que hacerla una vez, no es necesario repetirla para cada proyecto en que usemos git. Si algún día cambias de correo electrónico, simplemente vuelve a ejecutar el comando de arriba.

También debes saber que algunas veces git se comunica contigo usando un editor de texto en línea de comandos. Por defecto, utiliza `Vim` que es un editor que, si no lo conoces, puede resultar un poco difícil de manejar. Hay otro comando de configuración par cambiar el editor de texto por defecto. Por ejemplo, para cambiar a `nano`:

`git config --global core.editor nano`

Por último, para ver tu configuración, sólo tienes que ejecutar:

`git config --list`

## Iniciar el repositorio local

El repositorio local, como se comentó, es el directorio de mi disco duro donde está guardado mi proyecto. Para usar git con mi proyecto, el primer paso es iniciar git en el repositorio local. Esto se hace ejecutando:

`git init`

Muy importante, este comando se ejecuta **desde el directorio donde está alojado mi proyecto**. Y hay que iniciar git cada vez que empecemos un nuevo proyecto que queramos gestionar con git.

Al iniciar git se crea un subdirectorio oculto de nombre `.git` Este subdirectorio es para uso exclusivo del programa git, es decir, nosotros no tenemos que entrar en `.git` para nada, ni tenemos que hacer nada en él.

La carpeta donde tenemos alojado el proyecto podemos moverla a cualquier lugar del disco duro, incluso después de haber iniciado git en ella. Lo importante es que movamos la carpeta de proyecto en su totalidad, incluido el subdirectorio `.git` Podríamos moverla incluso si estuviese vinculada a un repositorio remoto.

## Vincular con un repositorio remoto

El repositorio local es obligatorio, porque git trabaja en tu ordenador, como ya se ha dicho varias veces. Pero si queremos hacer público nuestro proyecto en un repositorio remoto debemos seguir los siguientes pasos:

- Primero de todo, debemos tener cuenta en alguna web que ofrezca este servicio.
- En segundo lugar, hay que crear el repositorio en la web. No hay que confundir el alta en el servicio con el repositorio: Una vez tenemos cuenta en un servicio git en la web, podemos crear tantos repositorios como proyectos queramos compartir.
- En tercer y último lugar, hay que vincular el repositorio local con el repositorio remoto.

Este último paso se puede hacer de dos formas diferentes:

1. Crear ambos repositorios, el local y el remoto, acceder al repositorio local (donde hemos iniciado git) con la consola de comandos y ejecutar:  
`git remote add alias url`  
Recordemos que hay que identificar cada repositorio remoto con un nombre (alias) y que se usa el término **origin** para nombrar al repositorio remoto principal (o al único si sólo usamos uno). La *url* es la dirección web de nuestro repositorio remoto; esta dirección la conoceremos en el momento de crear el repositorio remoto.
2. Crear sólo el repositorio remoto pero no el local. También hay que iniciar el repositorio remoto; normalmente los servicios web te ofrecen la opción de iniciar git al crear el repositorio remoto. Entrar con la consola de comandos en el directorio de nuestro equipo que queremos que sea el repositorio local y ejecutar:  
`git clone url`  
La *url* es la del repositorio remoto. El comando `git clone` lo que hace es **clonar** el repositorio remoto en nuestro ordenador. Al clonarlo, se trae consigo el directorio oculto `.git` por lo que la clonación mata dos pájaros de un tiro: inicia git en el repositorio local y vincula los repositorios local y remoto. Por esto mismo, suele ser la forma por defecto en que se explica la vinculación a servicio git por internet.

El *alias* de un repositorio remoto se puede cambiar sin más que entrar en el repositorio local con la consola de comandos y ejecutar:

`git remote rename alias_viejo alias_nuevo`

## Hacer un commit

Un commit se hace en dos pasos. El primer paso es registrar los cambios y el segundo es pedirle a git que haga el commit. El registro de los cambios se denomina técnicamente **añadir los cambios al "staging area"** y se realiza con un simple comando. Una vez añadidos los cambios ya se puede hacer el commit, que se hace con otro comando:

`git add --all`  
`git commit -m "descriptor del commit"`

El segundo comando es el que realiza el *commit* propiamente dicho. Un commit hay que acompañarlo de una breve descripción de los cambios que se están registrando, cosa que se hace con el argumento `-m`. El texto se introduce entre comillas.

En realidad, ambos pasos pueden hacerse en uno, indicando el argumento `-a` al hacer el commit, tal que así:

`git commit -am "descriptor del commit"`

Pero los hemos separado en esta explicación, para poder hablar del **staging area**

### El staging area y la organización de los commits

El primer comando, el que añade los cambios a la *staging area*, se puede escribir de varias maneras. El argumento `--all` indica que se **añadan todos los archivos** al staging area. Pero hubiese sido suficiente con añadir aquel archivo que sufrió cambios; por lo tanto, el comando se podría haber escrito:

`git add nombre_archivo`

> Nota: el argumento `--all` tiene una segunda forma de ser escrito: con un simple *punto*. Es decir, escribir `git add .` es equivalente a escribir `git add --all`

Es decir, puedo añadir archivos a voluntad al *staging area*. Y es que la *staging area* es un espacio donde registro los archivos modificados, para luego confirmar todas estas modificaciones con un commit. Esto me permite organizar mis commits. Por ejemplo, estoy trabajando en dos funcionalidades distintas de un software. Y llega un punto que quiero registrar los cambios. Pues primero añado a la *staging area* los archivos que modifiqué con motivo de la primera funcionalidad y hago commit; y luego añado al *staging area* los archivos que modifiqué por la segunda funcionalidad y hago commit. He registrado mis cambios en dos commits, pero cada commit registra un aspecto diferente, lo que nos permite añadir más orden y claridad a la historia de cambios.

De hecho, del mismo modo que existen unos flujos de trabajo estandarizados, existen unas convenciones específicas sobre la confirmación de cambios, es decir, sobre los commits. En general, se hacen las siguientes recomendaciones:

- Un commit debe estar asociado a una tarea o subtarea específica.
- No se deben agrupar varias tareas en un solo commit.
- Sólo se deberían hacer commits de tareas terminadas, no de tareas parciales o incompletas.
- El commit debería incluir todos los archivos modificados al hacer la tarea.
- El commit sólo debería afectar a la tarea asociada y no a otras tareas.

El orden y claridad del historial de cambios se completa documentando bien los commits. Hemos visto que la opción `-m` permite documentar el commit con una frase sencilla. Pero si ejecutamos el commit sin esta opción, veremos que se nos abre el editor de texto en línea de comandos que [tenemos configurado](#instalar-y-configurar-git). Es decir, git no nos permite hacer un commit sin documentar, sin escribir algo sobre él.

Y como ocurre otras veces, también existen unas convenciones específicas para escribir los comentarios de un commit. En general, se hacen las siguientes recomendaciones:

- Comenzar con un título, de 50 caracteres como mucho. Redactar el título en modo imperativo.
- El título debe empezar con letra mayúscula y no se cierra con punto.
- Separar el título del cuerpo del mensaje con una línea en blanco.
- Escribir el cuerpo del mensaje en líneas de 72 caracteres de largo.
- Usar el cuerpo del mensaje para explicar el qué y el por qué del commit, pero no para explicar el cómo.

Obviamente, si la tarea es sencilla y se explica sólo con el título, utiliza la opción `-m` y pon sólo el título, sin más.

## Corregir un commit

Git tiene multitud de posibilidades para corregir commits. Este es un manual de manejo básico, por lo que sólo se explicará una mínima parte de todas estas opciones. Con esto podemos ir tirando hasta que acumulemos experiencia con git y queramos profundizar en el manejo.

### Con el comando reset

Vamos a ver dos ejemplos de uso del comando `git reset`. A continuación, los analizaremos para comprender las posibilidades que ofrece este comando.

Empezamos quitando un archivo del *staging area*; es decir, hemos añadido un archivo al *staging area*, con la intención de hacer un commit, pero nos hemos equivocado o arrepentido y lo queremos sacar:

`git reset HEAD nombre_archivo`

Los cambios que hayamos hecho al archivo **no se pierden**, simplemente sacamos el archivo del *staging area*. Fijémonos en que el comando utiliza el puntero *HEAD* como referencia.

Ahora vamos a volver a un commit anterior. Es decir, hemos hecho uno o varios commit pero nos hemos dado cuenta de un error o nos hemos arrepentido y queremos deshacer lo andado y volver a un punto anterior:

`git reset SHA_commit_destino`

En otras palabras, ejecutamos `git reset` indicando el commit al que queremos volver. Fijémonos que, a diferencia del caso anterior, la referencia es el SHA del commit.

Ahora, vamos a comparar estos dos casos que acabamos de ver profundizando en dos aspectos:

1. Cómo se hace la referencia
2. Qué elementos cambian

Respecto a la referencia, en un caso apuntamos a que nuestros cambios se hagan donde está *HEAD* y en el otro indicamos a qué commit queremos volver usando su *SHA*. La forma en que se hace la referencia no es tan relevante. Lo realmente importante es que indican en qué punto queremos hacer la corrección. De hecho, en el segundo ejemplo podríamos haber usado *HEAD* como referencia, pero tendríamos que haberlo usado así:

`git reset HEAD~2`

La virgulilla y el número indican el número de commits que quiero *borrar*. Así, por ejemplo, `git reset HEAD~2` borraría dos commits, el último y el penúltimo. Si quiero borrar sólo el último, escribiría `git reset HEAD~1`.

Entoces, ¿a dónde apuntamos cuando sólo indicamos *HEAD*? Pues si *HEAD~1* es el último commit, *HEAD* a secas debe ser [algo previo](#hacer-un-commit) al último commit. Es decir, es la carga de archivos a la *staging area*. Por eso, en el primer ejemplo sacamos los archivos que habíamos subido a la *staging area*.

Ahora profundizamos en el segundo aspecto, en qué elementos se ven afectados. Si nos fijamos en el primer caso, por la manera en que hemos referenciado nuestros cambios, sólo se pudo haber visto afectado el *staging area*. Pero en el segundo caso apuntamos a un commit anterior, lo que implica el borrado de los commits que haya por el medio. Es decir, git es capaz de operar sobre todo el commit o sólo sobre el *staging area*. Y esta es la clave: cuando hacemos un `git reset` le podemos decir a git si queremos que esos cambios apliquen sólo al *staging area*, a los archivos de nuestro repositorio local, o no toque nada, simplemente de para atrás al puntero *HEAD* sin tocar nada más.

Estos niveles de actuación se indican con un argumento, tal que así:

`git reset --soft SHA_commit_destino`  
`git reset --mixed HEAD~1`  
`git reset --hard HEAD~2`

En los ejemplos de arriba se utilizan deliberadamente distintas formas de referenciar. Saltan a la vista cuáles son los argumentos: `--soft`, `--mixed` y `--hard`. Existen más, pero nos centramos en estos tres, que son los más comunes.

Cuando empezamos este capítulo, el ejemplo no contenía ninguno de estos argumentos. En ausencia de argumento, git usa `--mixed`. Es decir, **`--mixed` es la opción por defecto**.

||`--soft`|`--mixed`|`--hard`|
|:---|:---:|:---:|:---:|
|mueve *HEAD*| sí | sí | sí |
|modifica *staging area*| no | sí | sí |
|modifica archivos en repositorio local| no | no | sí |

Como vemos, la opción por defecto borra el commit, es decir, borra el registro de cambios, pero no deshace los cambios: nos respeta los archivos tal y como están en el repositorio local. Si estamos convencidos que queremos devolver los archivos a la situación previa al commit, podemos usar la opción `--hard`.

### Con el comando revert

Una alternativa a `git reset` es el comando `git revert`. Este comando no borra commits. Lo que hace es añadir un commit que deshace lo realizado por el commit anterior. El uso del comando es igual que `reset`, pero sin argumentos, ya que no hay distintos niveles de `revert`. Por ejemplo:

`git revert HEAD~1`

Añade un commit que nos devuelve a como estábamos en un commit anterior. Es decir, desandamos el último commit.

`git revert HEAD~2`

Añade un commit que nos devuelve a como estábamos dos commits atrás. Es decir, desandamos el último y el penúltimo commit.

### Conclusiones

Modificar commit es posible, pero tiene repercusiones:

Si al hacer un commit sacamos a la luz alguna información que no debíamos hacer pública, el camino correcto es el `reset`, no el `revert`.

Hacer `reset` parece una opción más peligrosa, pero no es particularmente problemático; salvo un caso: Que hubiésemos **subido el commit que queremos borrar a un repositorio remoto**. Si estamos en este caso, la siguiente vez que quiera sincronizar mi repositorio local con el remoto, git me devolverá un error, ya que no coincidirán los commits. Por lo tanto, `reset` se puede usar cuando todavía no hemos subido los cambios a un repositorio remoto.

De todas formas, habría una solución a este caso que se acaba de plantear. Podríamos volver a igualar la secuencia de commits de los repositorios remoto y local siguiendo estos pasos:

1. En primer lugar, se haría una copia de los archivos del proyecto que tenemos en el repositorio local. Esa copia se guarda en un directorio temporal. No copiamos el directorio oculto `.git`, sólo nuestros archivos, los de nuestro proyecto.
2. En segundo lugar, nos descargamos el contenido del repositorio remoto mediante las herramientas de descarga de git. Esto iguala el contenido del repositorio remoto y el repositorio local.
3. Ahora borramos el contenido del repositorio local y pegamos la copia que guardamos en el directorio temporal.
4. Hacemos commit de este "gran" cambio que acabamos de hacer, y subimos el commit al repositorio remoto.

## Crear ramas y moverse entre ramas y commits

Crear ramas no tiene dificultad:

`git branch nombre_rama`

Para saber cuántas ramas tenemos o sobre qué rama estamos:

`git branch`

Devuelve un listado de las ramas e indica con un asterisco aquella sobre la que estamos.

Moverse a una rama tampoco es difícil:

`git checkout nombre_rama`

Usaremos el mismo comando para moverse a un commit:

`git checkout SHA_commit`

El comando `checkout`, cuando lo usamos con el nombre de una rama, nos coloca en el último commit de la rama. Este comando también arrastra al puntero *HEAD*, es decir, `git checkout nombre_rama` pone el puntero *HEAD* en el último commit de esa rama.

## <a name="obteniendo-informacion-el-log"></a> Obteniendo información. El log

Git dispone de un comando que nos permite ver la información de los commits y ramas de nuestro proyecto. Uno de los datos más importantes que podemos obtener es el SHA del commit:

`git log`

Ejecutado directamente, nos muestra un listado de todos los commits y sus comentarios. Sería equivalente a indicar la opción `git log --all`. Es decir, `--all` es la opción por defecto en la mayoría de los usos de `git log`. Si queremos la misma información, pero más resumida, tenemos la opción:

`git log --oneline`

Como su nombre indica, nos muestra un resumen de una línea con la información anterior. Si además no nos interesa todo el histórico de commits, si no simplemente lo que ocurrió desde un determinado commit en adelante:

`git log SHA_commit`

Si queremos ver los cambios concretos que sufrieron nuestros archivos, qué se añadió o qué se borró en cada commit, tenemos las opciones `--stat` y `--patch`. La primera opción muestra un informe de cambios (archivos modificados, qué linea se cambió, etc.) y la segunda opción muestra el contenido del archivo, el código fuente, con anotaciones para resaltar los cambios.

Por último, también tenemos la opción de mostrar la historia de commits y ramas en modo *gráfico*:

`git log --graph`

Como se indicó antes, la opción `--all` es la opción por defecto, luego `git log --graph` es equivalente a `git log --graph --all`. El gráfico gana el claridad si se ejecuta:

`git log --graph --oneline`

Como se ve, los argumentos de `git log` se pueden combinar de muchas formas.

## Subir y bajar del repositorio remoto

Una vez que [tenemos vinculados](#vincular-con-un-repositorio-remoto) nuestros repositorios local y remoto, subir nuestros commits es muy sencillo:

`git push alias_repositorio_remoto nombre_rama`

Si hemos seguido las recomendaciones de uso de nombres reservados en git, habitualmente escribiremos:

`git push origin main` para subir los commits de la rama principal *main*
`git push origin develop` para subir los commits de la rama de desarrollo *develop*

Al hacer `git push` nos pedirá nuestras credenciales en el servidor donde tenemos el repositorio remoto.

El ejercicio de descargar commits del repositorio remoto al local es idéntico pero con el comando `git pull`, es decir:

`git pull origin main` para descargarse el contenido actual de la rama *main* que haya en el repositorio remoto.

Si queremos sumarnos a un proyecto que ya está subido a un repositorio remoto, no hace falta que creemos el repositorio local, lo vinculemos con el remoto y hagamos un `git pull`, podemos hacer todo esto en un solo paso **clonando** el repositorio remoto en nuestro equipo:

`git clone url_repositorio_remoto`

## <a name="fusionar-ramas-hacer-merge"></a> Fusionar ramas. Hacer merge

Como ya [se explicó](#pasos-merge), un merge se hace en dos pasos. Primero hay que situarse en la rama que recibirá el contenido. Por ejemplo, imaginemos que fusionamos la rama *develop* en *main*; primer habría que situarse en *main*:

`git checkout main`

Y luego llamamos al contenido de *develop*:

`git merge develop`

A partir de aquí, [y según el caso](#three-way-merge), habría que pulir el proceso de *merge* trabajando en la rama *main*.

Si estuviésemos ante un escenario **fast forward** y ejecutamos *merge* sin más, tal y como se acaba de indicar, git ejecuta un fast forward merge. Si queremos omitir este comportamiento y generar un commit nuevo para registrar el merge, escribiremos:

`git merge --no-ff develop -m "descriptor del commit"`

Si no escribimos un descriptor del commit, se nos abrirá el editor de texto en línea de comandos que tengamos configurado, [exactamente igual que cuando hacemos un commit](#hacer-un-commit).

Si hemos hecho un fast forward merge, y queremos revertirlo para registar el merge con un commit, podemos hacer un reset *duro*:

`git reset --hard SHA_commit_donde_nació_la_rama_alternativa`

Para este reset debemos apuntar al commit donde nació la rama alternativa o, dicho de otra forma, al commit donde quedó parada la rama *main*.


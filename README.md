# Prólogo

Hay mucha información sobre git en internet. Y hay muchos manuales para principiantes. Pero, en general, toda esta información disponible suele omitir y dar por supuestos algunos conceptos básicos de git. Por lo tanto, para aquellos como yo que somos meros aficionados, comenzar con el uso de git es algo bastante difícil, a pesar de toda la documentación que existe.

Mi objetivo con este documento es introducir estas ideas básicas que se suelen omitir cuando se explica git. Con este manual, sin leer otra cosa, podrás empezar a usar git. Eso sí, de una manera muy básica. Pero para profundizar en tus conocimientos, como dije al principio, ya hay mucha información disponible.

Quiero señalar que este manual trata git desde cero, sin presuponer ningún conocimiento previo de git. Sin embargo, sí que presupone algún conocimiento general sobre informática. Es decir, este manual puede resultar igual de confuso que otros manuales si tu caso es el de una persona que se enfrenta por primera vez a un ordenador. Entre los conocimientos que se suponen, destaca que el lector o lectora sabe moverse por el árbol de directorios de su equipo usando la consola de comandos: entra y sale de directorios, sabe listar su contenido, crea y borra archivos y directorios, etc.

Por favor, que nadie espere un documento de gran rigor técnico, que mida exquisitamente cada palabra para que se ajuste a la ortodoxia informática. Está hecho por un aficionado y dirigido a aficionados y principiantes. Espero que valores mi trabajo como lo que es: un documento práctico para empezar con git. Por último recuerda que puedes copiarlo y distribuirlo libremente. Espero que te sea de utilidad.

Juan Antonio Silva Villar

## Versiones

- Marzo de 2019. Versión muy básica. Disponible sólo en pdf para impresión.
- Julio de 2022. Versión ampliada y cambio de título. Disponible en markdown para consulta en línea y mismo documento en pdf para impresión.
- Octubre de 2022. Se amplia la parte del fast forward merge.

## Licencia

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licencia de Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Este obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">licencia de Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional</a>.

Contacto del autor:  
[Juan Antonio Silva Villar](mailto:juan.silva@disroot.org)

El logotipo de git usado para ilustrar el repositorio se ha obtenido de la página [web oficial de git](https://git-scm.com/downloads/logos) y se utiliza en conformidad a la [licencia de estos logotipos](https://creativecommons.org/licenses/by/3.0/)


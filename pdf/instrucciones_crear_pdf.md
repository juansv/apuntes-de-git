# Introducción

La versión de julio de 2022 de mis apuntes de git está escrita en markdown. La ventaja del archivo markdown es que puede ser consultado online en el repositorio de gitlab. Pero si quieres imprimir mis apuntes en papel, necesitarás convertir el documento markdown a algún formato que permita imprimir sin los marcados propios de markdown. Una opción es el formato pdf.

Para facilitarte la misión, en el repositorio gitlab encontrarás mis apuntes también en pdf. Pero puede que el aspecto de mi documento pdf no sea de tu agrado y quieras hacerte tú tu propia edición de mis apuntes. En este documento te explico cómo convertir el documento markdown en pdf con otra hoja de estilos y cómo convertirlo a formato `.odt`, que te permitirá editar el documento con `libreoffice` u otro programa similar.

Como comentario final, estas instrucciones están hechas para usuarios de GNU/linux y se basan en herramientas de software libre. No te puedo prestar ayuda con otros sistemas operativos y herramientas, ya que no los manejo.

## Herramientas necesarias

La principal herramienta que se necesita es el programa `pandoc`. Este programa, que se maneja con la terminal, convierte archivos de documentación entre distintos formatos. Pandoc es capaz de hacer la conversión de markdown a pdf directamente.

Si queremos añadir una hoja de estilos que de forma al documento pdf final, también necesitaremos el programa `weasyprint`. Esta aplicación es llamada desde `pandoc` al crear el pdf, es decir, `pandoc` crea el documento pdf usando `weasyprint`. La ventaja de utilizar a `weasyprint` como *intermediario* es que `weasyprint` permite el uso de hojas de estilo para dar forma al documento pdf.

En mi caso particular, no tengo ni idea de hojas de estilo ni html, lo que me limita para conseguir efectos como la numeración de páginas. Este problema en particular lo resolví usando el programa `PDF Chain`. Este programa opera en el entorno gráfico y permite, entre otras cosas, *superponer* dos documentos pdf, como si fuesen páginas *transparentes*. Es decir, `PDF Chain` tiene una herramienta que trata los pdf como dos fotogramas, pone uno encima del otro, y el resultado es la mezcla de ambos. De esta forma, por un lado creé el pdf sin números de página con `pandoc`, `weasyprint` y mi hoja de estilo y por otro un documento con la misma cantidad de hojas pero con los números de página como único contenido. Al superponer ambos documentos, obtuve un único pdf con el contenido de ambos, página a página. En este repositorio tienes dos capturas de pantalla, una de los créditos del programa `PDF Chain`, para que lo identifiques en tu distribución GNU/Linux, y otro que ilustra el uso de la herramienta *stamp*, que permite la *superposición* de pdf. Es muy intuitivo, de un simple vistazo a la captura de pantalla lo entenderás.

El documento pdf con los números de página lo generé con `libreoffice writer`. En el repositorio dejo también tanto el archivo `.odt` como el pdf resultante.

Respecto a las hojas de estilo, me he aprovechado del trabajo de **Jason Milkins**, que tiene publicadas varias `css` en el siguiente repositorio:

<https://jasonm23.github.io/markdown-css-themes/>

Este excelente trabajo está bajo la misma licencia Creative Commons con la que he liberado mi manual, lo que cumple con unos de los requisitos para usar estas `css`. Los otros requisitos son la mención del autor, cosa que ya hice en el párrafo anterior. Y el último requisito es mencionar los cambios que realicé: En el repositorio de Jason Milkins hay varias hojas de estilo destinadas al uso en documentos markdown. Yo he utilizado la hoja de estilo de nombre `markdown5` como base para mi pdf. Le he realizado varias modificaciones, para adaptarla a mi gusto. En concreto los tamaños de fuente, la inserción de un salto de página antes de los encabezados de nivel `h1` y cambié el formato de los `code blocks` de markdown para poner otros de mi gusto.

## Procedimiento

Si no tienes intención de usar ninguna hoja de estilos, la conversión de markdown a pdf con `pandoc` es tan sencilla como ejecutar:

`pandoc -f markdown -t pdf nombre_archivo.md -o nombre_archivo.pdf`

Las opciones son:

`-f` que indica *from*, es decir, el formato de origen.  
`-t` que indica *to*, es decir, el formato de destino.  
`-o` que indica *output*, es decir, el archivo de salida.

Puedes observar que el uso de `pandoc` es muy sencillo.

Si quieres utilizar una hoja de estilo, la cosa se complica un poco. No es posible aplicar hojas de estilo a un documento markdown. Pero sí en un documento html. Y la conversión markdown a html es muy sencilla (markdown nació como un html "simplificado"). Por lo tanto, se le puede aplicar una hoja de estilo a nuestro documento markdown haciendo una conversión intermedia a html. Esto lo podemos resolver con una *pipe* tal que así:

`pandoc -f markdown -t html nombre_archivo.md | pandoc -f html -t pdf --pdf-engine=weasyprint --css=/ruta_al_archivo_css -o nombre_archivo.pdf`

En este caso, hay dos nuevas opciones:

`--pdf-engine=weasyprint` que le indica a `pandoc` que utilice como "motor" de creación de pdf el programa `weasyprint`. Recordemos que, [como ya indiqué](#herramientas-necesarias), este programa admite el uso de hojas de estilo.

`--css=/ruta_al_archivo_css` que le indica la hoja de estilo que quiero aplicar.

En mi caso particular, también quería que mi documento pdf tuviese *metadatos*. Estos metadatos los recogí en un archivo `yaml`, disponible en el repositorio. Para generar el pdf con metadatos, hay que añadir la opción `--metadata-file=ruta_al_archivo_yaml` en la segunda parte de la *pipe* anterior.

Con todo lo explicado hasta aquí, tienes información suficiente para generar el documento pdf con el aspecto que tú prefieras. No obstante, voy a hacer dos comentarios más...

## Edición del documento html para mejorar el formato

En mi caso, no me gustaba el resultado final que se obtenía con la *pipe*, por lo que opté por hacer la conversión en tres pasos separados:

1. Primero, crear un documento html con el comando `pandoc -f markdown -t html nombre_archivo.md -o nombre_archivo.html`

2. Luego edité el documento html para conseguir que en el siguiente paso el archivo pdf tuviese el aspecto que realmente quería que tuviese.

3. Por último, creé el archivo pdf con el comando `pandoc -f html -t pdf --pdf-engine=weasyprint --css=/ruta_al_archivo_css --metadata-file=ruta_al_archivo_yaml nombre_archivo.html -o nombre_archivo.pdf`

Lo que no me gustaba del archivo pdf cuando lo creaba con la *pipe*, es que `pandoc`, cuando se insertan metadatos en el pdf, crea una primera página con un resumen de estos metadatos. La solución que se me ocurrió fue utilizar esta primera hoja que inserta `pandoc` como portada para mi documento pdf. Pero esto me obligaba a unos arreglos en el documento html antes de convertir a pdf. En concreto, la edición que realicé al archivo html fue la siguiente:

- Quité la referencia a mi autoría que sale en la línea `<p class="author">Juan Antonio Silva Villar</p>` ya que mi nombre ya se inserta en la primera página del pdf por el hecho de estar en los metadatos. Si dejase esta segunda mención, mi nombre saldría dos veces, lo que resulta raro.

- Añadí el código `<h1 id="hoja en blanco"> </h1>` en los distintos puntos del código html donde quería insertar saltos de página o páginas en blanco. Los saltos y páginas en blanco resuelven la maquetación del documento a la hora de imprimirlo.

## Alternativas

Si no quieres perder tiempo editando html, hojas de estilo y demás, y te sientes más cómodo con un editor de texto avanzado tipo `libreoffice writer`, puedes convertir de markdown a `.odt` con pandoc:

`pandoc -f markdown -t odt nombre_archivo.md -o nombre_archivo.odt`

Y editas el `.odt` resultante con `libreoffice` a tu gusto. De esta forma también resuelves, en una misma jugada, la inserción de los números de página.

